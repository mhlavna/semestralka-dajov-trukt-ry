package model;

import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;
import java.io.Serializable;

/**
 * @author Martin Hlavňa
 * @version 1.0
 * @created 02-4-2015 21:22:55
 */
public class Reader implements Serializable {

    private int aAge;
    private LinkedList<Book> aBorrowedBooks;
    private int aBorrowsTotalCount;
    private String aFirstName;
    private String aLastName;
    private int aCountBooksIamWaitingFor;

    /**
     * Constructor creates new object of class Reader
     *
     * @param paAge int Age of the reader
     * @param paFirstName String First name of the reader
     * @param paLastName String Last name of the reader
     */
    public Reader(int paAge, String paFirstName, String paLastName) {
        this.aAge = paAge;
        this.aFirstName = paFirstName;
        this.aLastName = paLastName;
        this.aBorrowedBooks = new LinkedList<>();
        this.aBorrowsTotalCount = 0;
        this.aCountBooksIamWaitingFor = 0;
    }

    /**
     * Adds new book to the collection of the borrowed books
     *
     * @param paBook Borrowed book
     * @throws LibraryException When there is an error in underlaying structure
     */
    public void addBook(Book paBook) throws LibraryException {
        try {
            this.aBorrowedBooks.add(paBook);
            aBorrowsTotalCount++;
        } catch (EList ex) {
            throw new model.LibraryException("Error in adding book to the LinkedList caused by: " + ex.getMessage());
        }
    }

    /**
     * Deletes book from Reader's borrowed books (when Reader returns the book)
     *
     * @param paBook
     * @throws LibraryException When there is an error in underlaying structure
     */
    public void deleteBook(Book paBook) throws LibraryException {
        try {
            this.aBorrowedBooks.delete(paBook);
        } catch (EList ex) {
            throw new model.LibraryException("Error in removing book from the LinkedList caused by: " + ex.getMessage());
        }
    }

    /**
     * Returns age of the Reader
     *
     * @return int Age of the reader
     */
    public int getAge() {
        return aAge;
    }

    /**
     * Returns list of the borrowed books
     *
     * @return LinkedList<Book> List of the books borrowed by this reader
     */
    public LinkedList<Book> getBorrowedBooks() {
        return aBorrowedBooks;
    }

    /**
     * Returns number of the total borrowed books
     *
     * @return Int Number of the TOTAL borrowed books
     */
    public int getBorrowedBooksTotal() {
        return aBorrowsTotalCount;
    }

    /**
     * Returns number of the curently borrowed books
     *
     * @return Int Number of the CURRENTLY borrowed books
     */
    public int getBorrowedBooksCount() throws LibraryException {
        try {
            return aBorrowedBooks.size();
        } catch (EList ex) {
            throw new LibraryException("Problem getting borrowed books count caused by: " + ex.getMessage());
        }
    }

    /**
     * Returns first name of the Reader
     *
     * @return String First name of the reader
     */
    public String getFirstName() {
        return aFirstName;
    }

    /**
     * Returns last name of the Reader
     *
     * @return String last name of the user
     */
    public String getLastName() {
        return aLastName;
    }

    /**
     *
     * @param paAge
     */
    public void setAge(int paAge) {
    }

    /**
     * Sets the first name of the Reader (when e.g. he changes his name)
     *
     * @param paName String New name of the Reader
     */
    public void setFirstName(String paName) {
        this.aFirstName = paName;
    }

    /**
     * Sets the Reader's last name (surname) (when e.g. Woman marries, of
     * changes last name )
     *
     * @param paName String New Last name
     */
    public void setLastName(String paName) {
        this.aLastName = paName;
    }

    /**
     * Increases number of books Reader is waiting for by 1;
     */
    public void addWaitedBook() {
        aCountBooksIamWaitingFor++;
    }

    /**
     * Decreases number of books reader is waitng for by 1;
     */
    public void subWaitedBook() {
        aCountBooksIamWaitingFor--;
    }

    /**
     * Returns number of books user is waiting for
     *
     * @return number of books user is waiting for
     */
    public int getWaitedBooksCount() {
        return aCountBooksIamWaitingFor;
    }

    /**
     * Checks if user have book
     *
     * @param paBook Book to check
     * @return true if yes false if no
     * @throws LibraryException if there is problem with underlaying structure
     */
    public boolean haveBook(Book paBook) throws LibraryException {
        try {
            if (this.aBorrowedBooks.indexOf(paBook) > -1) {
                return true;
            }
        } catch (EList ex) {
            throw new LibraryException("Error while checking user books caused by: " + ex.getMessage());
        }

        return false;
    }

    @Override
    public String toString() {
        return aFirstName + " " + aLastName;
    }
}
