package model;

import datastructures.lists.EList;
import datastructures.lists.LinkedList.SortedLinkedList;
import java.io.Serializable;

/**
 * @author Martin Hlavňa
 * @version 1.0
 * @created 02-4-2015 21:22:54
 */
public class BookGenre implements Serializable {

    private SortedLinkedList<Book> aBooks;
    private String aName;

    /**
     * Constructor creates new instance of the BookGenre class
     *
     * @param paName Name of the genre. It wouldbe wise to have it unique in the
     * entire application but is not neccesesary
     */
    public BookGenre(String paName) {
        this.aName = paName;
        this.aBooks = new SortedLinkedList<>();
    }

    /**
     * Returns collection of the books ordered by Author and name that bellongs
     * to this genre
     *
     * @return LinkedList<Book> List of the books in this genre
     */
    public SortedLinkedList<Book> getBooks() {
        return aBooks;
    }

    /**
     * Returns name of the genre
     *
     * @return String name of the genre
     */
    public String getName() {
        return aName;
    }

    /**
     * Sets the name of the genre
     *
     * @param paName String New name of the genre
     */
    public void setName(String paName) {
        this.aName = paName;
    }

    /**
     * Adds book to the genre and sorts list by Author and Book Name
     *
     * @param paBook Book to add
     * @throws LibraryException If there was a problem in the underlying
     * structure
     */
    public void addBook(Book paBook) throws LibraryException {
        try {
            this.aBooks.add(paBook);

        } catch (EList ex) {
            throw new LibraryException("Error adding book to the genre caused by: " + ex.getMessage());
        }
    }

    /**
     * Removes book from the genre. If book is removed it it means the remaining
     * list is still sorted so no sorting action is needed here. If book is not
     * present in the list, then nothing happens
     *
     * @param paBook Book to remove
     * @throws LibraryException If there was a problem in the underlying
     * structure
     */
    public void deleteBook(Book paBook) throws LibraryException {
        try {
            this.aBooks.delete(paBook);
        } catch (EList ex) {
            throw new LibraryException("Error deleting book from the genre caused by: " + ex.getMessage());
        }
    }

    @Override
    public String toString() {
        return aName;
    }
}
