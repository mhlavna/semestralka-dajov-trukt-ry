/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Exception for errors in application model classes
 *
 * @author Martin Hlavňa
 */
public class LibraryException extends Exception {

    /**
     * Constructs an instance of
     * <code>LibraryException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public LibraryException(String msg) {
        super(msg);
    }
}
