package datastructures.lists.LinkedList;

import datastructures.lists.EList;
import java.io.Serializable;

/**
 * Modification of Linked List that ensures added elements are automatically
 * sorted
 *
 * @author Martin Hlavňa
 */
public class SortedLinkedList<E extends Comparable<E>> extends LinkedList<E> implements Serializable {

    @Override
    public void insert(E paElement, int paIndex) throws EList {
        throw new EList("Inserting at index is not allowed in SortedLinkedList implementation of LinkedList as it would have no effect after sorting");
    }

    @Override
    public void add(E paElement) throws EList {
        LinkedListChunk<E> newChunk = new LinkedListChunk<>(paElement);
        if (aLastChunk == null) {
            aFirstChunk = aLastChunk = newChunk;
        } else {
            if (aFirstChunk.getData().compareTo(paElement) > 0) {
                newChunk.setNext(aFirstChunk);
                aFirstChunk = newChunk;
            } else {
                if (aFirstChunk.getNext() == null) {
                    aFirstChunk.setNext(newChunk);
                    aLastChunk = newChunk;
                } else {
                    LinkedListChunk<E> chunk, tmpChunk;
                    chunk = aFirstChunk;
                    while (chunk.getNext() != null) {
                        tmpChunk = chunk.getNext();
                        if (tmpChunk.getData().compareTo(paElement) > 0) {
                            chunk.setNext(newChunk);
                            newChunk.setNext(tmpChunk);
                            break;
                        } else {
                            chunk = tmpChunk;
                        }
                    }
                    //došli sme na koniec
                    chunk.setNext(newChunk);
                    aLastChunk = newChunk;
                }
            }
        }


        aCount++;
    }
}
