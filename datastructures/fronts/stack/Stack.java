package datastructures.fronts.stack;

import datastructures.fronts.AbstractFront;
import datastructures.fronts.EFront;
import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;
import java.io.Serializable;

/**
 * Implementation of the Stack using linked list
 *
 * @author Martin Hlavňa
 */
public class Stack<E> extends AbstractFront<E> implements Serializable {

    public Stack() {
        super(new LinkedList<E>());
    }

    @Override
    public E pop() throws EFront {
        try {
            return aList.deleteFromIndex(this.size() - 1);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public E peek() throws EFront {
        try {
            if (this.size() > 0) {
                return aList.get(this.size() - 1);
            }
            return null;
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public void push(E paElement) throws EFront {
        try {
            aList.add(paElement);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }
}
