/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.fronts;

import datastructures.lists.EList;
import datastructures.lists.IList;
import java.io.Serializable;
import java.util.Iterator;

/**
 *
 * @author hlavna2
 */
public abstract class AbstractFront<E> implements IFront<E>, Serializable {

    protected IList<E> aList;

    protected AbstractFront(IList<E> aList) {
        this.aList = aList;
    }

    @Override
    public boolean isEmpty() throws EFront {
        return this.size() == 0;
    }

    @Override
    public int size() throws EFront {
        try {
            return aList.size();
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public void clear() throws EFront {
        try {
            aList.clear();
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public Iterator<E> iterator() {
        return aList.iterator();
    }
}
