package model;

import datastructures.fronts.EFront;
import datastructures.fronts.stack.Stack;
import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;
import datastructures.lists.LinkedList.SortedLinkedList;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author Martin Hlavňa
 * @version 1.0
 * @created 02-4-2015 21:22:55
 */
public class Library implements Serializable {

    private LinkedList<Book> aBookBorrowsFront;
    private SortedLinkedList<Book> aBooks;
    private LinkedList<BookGenre> aGenres;
    private LinkedList<Reader> aReaders;
    private Stack<Book> aReturnedBooks;
    private static Library instance = null;

    /**
     * Constructor, creates new instance of the library.
     */
    private Library() {
        this.aBooks = new SortedLinkedList<>();
        this.aGenres = new LinkedList<>();
        this.aReaders = new LinkedList<>();
        this.aReturnedBooks = new Stack<>();
    }

    /**
     * Singleton static call to get instance of the Library. If instance has not
     * yet been created then it creates a new one
     *
     * @return Library Instance of the class
     */
    public static Library getInstance() {
        if (instance == null) {
            instance = new Library();
        }
        return instance;
    }

    /**
     * Adds book to the Library
     *
     * @param paAuthor Author of the book
     * @param paBookName Name of the book
     * @param paPublicationYear Year when the book was published
     */
    public Book addBook(String paAuthor, String paBookName, int paPublicationYear) throws LibraryException {
        try {
            Book book = new Book(paBookName, paAuthor, paPublicationYear);
            aBooks.add(book);
            return book;
        } catch (EList ex) {

            throw new model.LibraryException("Error adding book to the library caused by: " + ex.getMessage());

        }
    }

    /**
     * Adds genre to the book in Library
     *
     * @param paGenre Genre of the book. Needs to be created first.
     * @param paBook Book to add genre to
     *
     * @throws LibraryException If there is a problem with underlying structures
     */
    public void addBookGenre(BookGenre paGenre, Book paBook) throws LibraryException {
        paGenre.addBook(paBook);
        paBook.addGenre(paGenre);
    }

    /**
     * Adds (registers) new reader to the Library
     *
     * @param paFirstName First name of the reader
     * @param paLastName Last name of the reader (surname)
     * @param paAge Age of the Reader
     *
     * @throws LibraryException If there is a problem with the underlying
     * structures
     */
    public Reader addReader(String paFirstName, String paLastName, int paAge) throws LibraryException {
        try {
            Reader reader = new Reader(paAge, paFirstName, paLastName);
            this.aReaders.add(reader);
            return reader;
        } catch (EList ex) {
            throw new LibraryException("Error adding Reader to the Library caused by: " + ex.getMessage());
        }
    }

    /**
     * User borrows a book
     *
     * @param paReader Reader that wants to borrow a book
     * @param paBook Book To be borrowed
     *
     * @return true if book has been borrowed false when book has been pushed to
     * the Waiting list only
     */
    public boolean borrowBook(Reader paReader, Book paBook) throws LibraryException {
        if (paBook.isInShelf()) {
            paReader.addBook(paBook);
            paBook.setBorrowedBy(paReader);
            return true;
        }
        paBook.addWaiter(paReader);
        paReader.addWaitedBook();
        return false;
    }

    /**
     * Finds book by name
     *
     * @param paName String name of the book
     */
    public Book findBookByName(String paName) {
        for (Book book : aBooks) {
            if (paName.toLowerCase().equals(book.getName().toLowerCase())) {
                return book;
            }
        }
        return null;
    }

    /**
     * Finds reader that currently have most books borrowed
     *
     * @return Reader that have most books borrowed, null if no Books are
     * borrowed at the moment
     */
    public Reader findCurrentMostActiveReader() throws LibraryException {

        Reader max = null;
        int maxval = -1;
        for (Reader reader : aReaders) {
            if (maxval < reader.getBorrowedBooksCount()) {
                max = reader;
                maxval = reader.getBorrowedBooksCount();
            }
        }

        return max;
    }

    /**
     * Finds reader that have borrowed most books so far
     *
     * @return Reader that have most books borrowed in total null if no books
     * have been ever borrowed
     */
    public Reader findMostActiveReader() throws LibraryException {
        Reader max = null;
        int maxval = -1;
        for (Reader reader : aReaders) {
            if (maxval < reader.getBorrowedBooksTotal()) {
                max = reader;
                maxval = reader.getBorrowedBooksTotal();
            }
        }

        return max;
    }

    /**
     * Finds Reader that have currently most books in Waiting list
     *
     * @return Reader with most books in Waiting list null if there are no books
     * in waiting list
     */
    public Reader findMostWaitingReader() {
        Reader max = null;
        int maxval = -1;
        for (Reader reader : aReaders) {
            if (maxval < reader.getWaitedBooksCount()) {
                max = reader;
                maxval = reader.getWaitedBooksCount();
            }
        }

        return max;
    }

    /**
     * Return list of genres book sorted by Author and Name of the book
     *
     * @param paGenre Genre to get list of books, if null, then all books are
     * returned. If genre is not in the list then exception is thrown
     *
     * @throws LibraryException If genre is not in the library or there is a
     * problem with the underlying structure
     */
    public SortedLinkedList<Book> getGenreBooks(BookGenre paGenre) throws LibraryException {
        if (paGenre == null) {
            return aBooks;
        } else {
            try {
                if (this.aGenres.indexOf(paGenre) > -1) {
                    return paGenre.getBooks();
                } else {
                    throw new LibraryException("Suplied genre is not in the Library!");
                }
            } catch (EList ex) {
                throw new LibraryException("Error in finding genre in the list caused by: " + ex.getMessage());
            }
        }

    }

    /**
     * Get list of the genres
     *
     * @return List of the genres
     */
    public LinkedList<BookGenre> getGenres() {
        return aGenres;
    }

    public int getGenresCount() throws LibraryException {
        try {
            return aGenres.size();
        } catch (EList ex) {
            throw new LibraryException("Error retireving count of genres caused by: " + ex.getMessage());
        }
    }

    /**
     * Get list of the readers
     *
     * @return List of the readers
     */
    public LinkedList<Reader> getReaders() {
        return aReaders;
    }

    /**
     * Loads library state from file
     *
     * @param paFilename Name of the file to load state
     * @throws LibraryException if there is a problem reading from a file
     */
    public static Library getInstance(String paFilename) throws LibraryException {
        try {

            FileInputStream fin = new FileInputStream(paFilename);
            ObjectInputStream ois = new ObjectInputStream(fin);
            Library tmp = (Library) ois.readObject();
            ois.close();
            //aby sa nahodou do instace neulozil nejaky chybny objekt
            instance = tmp;
            return instance;

        } catch (IOException | ClassNotFoundException ex) {
            throw new LibraryException("Error while reading from a file caused by" + ex.getMessage());
        }
    }

    /**
     * Books are being processed in order they have been returned. If there is a
     * Record in waiting list for the book according to the priorities, then it
     * is borrowed ASAP, otherwise book will be returned to the shelf
     */
    public void proccessReturnedBooks() throws LibraryException {
        Book book;
        try {
            while (aReturnedBooks.peek() != null) {
                book = aReturnedBooks.pop();
                book.giveToTheNextWaiter();

            }
        } catch (EFront ex) {
            throw new LibraryException("Error while proccessing books caused by: " + ex.getMessage());
        }
    }

    /**
     * Permanently delete book from Library
     *
     * @param paBook Book to remove
     * @throws LibraryException If there is a problem with underlying structure
     */
    public void removeBook(Book paBook) throws LibraryException {
        try {
            Book bookToDelete = this.aBooks.delete(paBook);
            for (BookGenre genre : bookToDelete.getGenres()) {
                this.removeBookGenre(paBook, genre);
            }

        } catch (EList ex) {
            throw new LibraryException("Error while deleting book from Library caused by: " + ex.getMessage());
        }
    }

    /**
     * Removes book from genre. If book is not present in genre nothing happens
     *
     * @param paBook Book to delte
     * @param paGenre genre to delete from
     *
     * @throws LibraryException If there is problem with underlying structures
     */
    public void removeBookGenre(Book paBook, BookGenre paGenre) throws LibraryException {
        paGenre.deleteBook(paBook);
        paBook.removeGenre(paGenre);
    }

    /**
     * Permanently remove Reader from Library. Reader must have all books
     * returned.
     *
     * @param paReader Reader to delete
     * @throws LibraryException If there is problem with the underlying
     * structure or user have not yet retured all the books
     */
    public void removeReader(Reader paReader) throws LibraryException {
        if (paReader.getBorrowedBooksCount() > 0) {
            throw new LibraryException("Cannot delete user, he has not returned all the books yet");
        } else {
            try {
                aReaders.delete(paReader);
            } catch (EList ex) {
                throw new LibraryException("Error while deleting reader caused by: " + ex.getMessage());
            }
        }
    }

    /**
     * Return book to the library. If book is not borrowed nothing happens
     *
     * @param paBook Book to return
     * @throws LibraryException If there is problem with the underlying
     * structure
     */
    public void returnBook(Book paBook) throws LibraryException {
        if (paBook.isInShelf()) {
            return; //book already returned no action
        }
        try {
            this.aReturnedBooks.push(paBook);
            paBook.getBorrowedBy().deleteBook(paBook);
        } catch (EFront ex) {
            throw new LibraryException("Error while returning book caused by: " + ex.getMessage());
        }
    }

    /**
     * Saves library state to the file
     *
     * @param paFilename Name of the file to save state
     * @throws LibraryException if there is problem while saving file
     */
    public void saveState(String paFilename) throws LibraryException {
        try {
            FileOutputStream fout = new FileOutputStream(paFilename);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
            oos.close();
        } catch (Exception ex) {

            throw new LibraryException("Error while saving file caused by: " + ex.getMessage());

        }
    }

    /**
     * Creates a new genre;
     *
     * @param paName Name of the genre
     */
    public BookGenre createGenre(String paName) throws LibraryException {
        try {
            BookGenre genre = new BookGenre(paName);
            this.aGenres.add(genre);
            return genre;
        } catch (EList ex) {
            throw new LibraryException("Error while creating genre caused by: " + ex.getMessage());
        }
    }

    /**
     * Gets count of returned books
     *
     * @return Count of returned books
     * @throws LibraryException If there is problem with underlying structure
     */
    public int getReturnedBooksCount() throws LibraryException {
        try {
            return aReturnedBooks.size();
        } catch (EFront ex) {
            throw new LibraryException("Error while checking count of returned books caused by: " + ex.getMessage());
        }
    }
}
