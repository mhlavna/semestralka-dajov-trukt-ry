package model;

import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;
import datastructures.priorityFronts.EPriorityFront;
import datastructures.priorityFronts.PriorityFront;
import java.io.Serializable;

/**
 * @author Martin Hlavňa
 * @version 1.0
 * @created 02-4-2015 21:22:54
 */
public class Book implements Comparable<Book>, Serializable {

    private String aAuthor;
    private Reader aBorrowedBy;
    private LinkedList<BookGenre> aGenres;
    private String aName;
    private int aPublicationYear;
    private boolean inShelf;
    private PriorityFront<Reader> aWaitingList;

    /**
     * Constructor creates a new instance of Book, Book by default dont have any
     * genres and is in shelf, also is not borrowed by anyonce
     *
     * @param paName Name of the book
     * @param paAuthor Name of the book author
     * @param paPublicationYear Year, when book was published
     */
    public Book(String paName, String paAuthor, int paPublicationYear) {
        this.aAuthor = paAuthor;
        this.aName = paName;
        this.aPublicationYear = paPublicationYear;
        this.aBorrowedBy = null;
        this.aGenres = new LinkedList<>();
        this.inShelf = true;
        this.aWaitingList = new PriorityFront<>();

    }

    /**
     * Checks if Book have genre
     *
     * @param paGenre Genre To Check
     * @return true if yes false if no
     * @throws LibraryException If there is a problem with underlying structure
     */
    public boolean haveGenre(BookGenre paGenre) throws LibraryException {
        try {
            if (aGenres.indexOf(paGenre) > -1) {
                return true;
            }
        } catch (EList ex) {
            throw new LibraryException("Error while checking genre of book caused by: " + ex.getMessage());
        }
        return false;

    }

    /**
     * Adds genre to the list of genres
     *
     * @param paGenre Genre to add
     * @throws LibraryException if problem
     */
    public void addGenre(BookGenre paGenre) throws LibraryException {
        try {
            this.aGenres.add(paGenre);
        } catch (EList ex) {
            throw new model.LibraryException("Error adding genre to LinkedList caused by: " + ex.getMessage());
        }
    }

    /**
     * returns String with name of book author
     *
     * @return String name of the author
     */
    public String getAuthor() {
        return aAuthor;
    }

    /**
     * Returns instance of Reader class that currently has this book borrowed
     *
     * @return Reader if any, null if book is in shelf or readz to be checked
     */
    public Reader getBorrowedBy() {
        return aBorrowedBy;
    }

    /**
     * Gets list of the genres this book have
     *
     * @return LinkedList<BookGenre> list of book's genres
     */
    public LinkedList<BookGenre> getGenres() {
        return aGenres;
    }

    /**
     * Returns name of the book
     *
     * @return String name of the book
     */
    public String getName() {
        return aName;
    }

    /**
     * Gets the year book has been published in
     *
     * @return int reprezentation of the year
     */
    public int getPublicationYear() {
        return aPublicationYear;
    }

    /**
     * If book is in shelf, then it is ready to be borrowed. If function returns
     * false, then book is either:
     *
     * - borrowed or returned but needs to be checked
     *
     * @return
     */
    public boolean isInShelf() {
        return inShelf;
    }

    /**
     * Removes genre from the book, should be used only if you add genre by
     * mistake
     *
     * @param paGenre Genre to remove, if it is not present nothing happens
     */
    public void removeGenre(BookGenre paGenre) throws LibraryException {
        try {
            this.aGenres.delete(paGenre);
        } catch (EList ex) {
            throw new LibraryException("Error when deleting genre from LinkedList caused by: " + ex.getMessage());
        }
    }

    /**
     * Sets reader that have this book borrowed
     *
     * @param paReader Reader that borrowed this book
     */
    public void setBorrowedBy(Reader paReader) {
        this.aBorrowedBy = paReader;
        this.inShelf = false;
    }

    /**
     * Sets name of the book. Should be used only when you passed incorrect name
     * in the constructor
     *
     * @param paName String New name of the book
     */
    public void setName(String paName) {
        this.aName = paName;
    }

    /**
     * Compares book with another first by author and then by name.
     *
     * @param paBook Book to compare with
     * @return -1 if this book is "smaller" than book passed via parameter, 0 if
     * they are equal, and 1 if this book is "greather" than book in parameter
     */
    @Override
    public int compareTo(Book paBook) {
        int cmpAuthor = aAuthor.compareToIgnoreCase(paBook.aAuthor);
        if (cmpAuthor == 0) {
            return aName.compareToIgnoreCase(paBook.aName);
        } else {
            return cmpAuthor;
        }
    }

    /**
     * Adds reader to the waiting list
     *
     * @param paReader Reader that will borrow book in the future
     * @throws LibraryException If there is a problem with the underlying
     * structure
     */
    public void addWaiter(Reader paReader) throws LibraryException {
        int priority = 2;
        if (paReader.getAge() < 26) {
            priority = 1;
        }
        if (paReader.getAge() > 60) {
            priority = 0;
        }
        try {
            this.aWaitingList.push(paReader, priority);
        } catch (EPriorityFront ex) {
            throw new LibraryException("Error when adding Waiter to the book caused by: " + ex.getMessage());
        }

    }

    /**
     * Sets borrowed by to most priority reader (if multiple readers with the
     * same priority exists, then the first will be chosen). If there are no
     * waiter, nothing happens
     *
     * @return false if there are no waiters true if borrowing book automaticaly
     * succeded
     */
    public boolean giveToTheNextWaiter() throws LibraryException {
        try {
            if (aWaitingList.size() > 0) {
                aBorrowedBy = aWaitingList.pop();
                aBorrowedBy.subWaitedBook();
                aBorrowedBy.addBook(this);
                return true;
            }
            this.inShelf = true;
            return false;
        } catch (EPriorityFront ex) {
            throw new LibraryException("Error when processing Waiter caused by: " + ex.getMessage());
        }
    }

    @Override
    public String toString() {
        return aName;
    }
}
