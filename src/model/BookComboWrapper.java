package model;

/**
 * Wrapper to use books in combo boxes with custom label
 *
 * @author Martin Hlavňa
 */
public class BookComboWrapper {

    private Book book;

    /**
     * Creates new wrapper
     *
     * @param book
     */
    public BookComboWrapper(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return this.book.getAuthor() + ": " + this.book.getName();
    }

    /**
     * Returns original object
     *
     * @return Book that has been wrapped
     */
    public Book getBook() {
        return book;
    }
}
