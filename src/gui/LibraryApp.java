/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import datastructures.lists.LinkedList.LinkedList;
import datastructures.lists.LinkedList.SortedLinkedList;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import model.Book;
import model.BookComboWrapper;
import model.BookGenre;
import model.Library;
import model.LibraryException;
import model.Reader;

/**
 *
 * @author Coder
 */
public class LibraryApp extends javax.swing.JFrame {

    private Library aLibrary;
    private boolean openDialog;
    private boolean saveDialog;
    private BookGenre selectedGenre;
    private Reader aLogedReader;
    private Book selectedBook;

    /**
     * Creates new form LibraryApp
     */
    public LibraryApp() {
        this.selectedGenre = null;
        this.saveDialog = false;
        this.openDialog = false;

        initComponents();
        aSaveMenu.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.Event.CTRL_MASK));
        aOpenMenu.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.Event.CTRL_MASK));
        fileChoserPopup.setLocationRelativeTo(null);
        addBookPopup.setLocationRelativeTo(null);
        addBookGenrePopup.setLocationRelativeTo(null);
        bookGenresPopup.setLocationRelativeTo(null);
        addReaderPopup.setLocationRelativeTo(null);
        userBookPopup.setLocationRelativeTo(null);

        aLibrary = Library.getInstance();

    }

    private void printReaders() {
        LinkedList<Reader> readers = aLibrary.getReaders();
        Object[] row = new Object[4];
        row[0] = "Meno";
        row[1] = "Vek";
        row[2] = "";
        row[3] = "";
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.setColumnIdentifiers(row);
        for (Reader reader : readers) {
            row = new Object[4];
            row[0] = reader;
            row[1] = reader.getAge();
            row[2] = "Knihy";
            row[3] = "Vymazať";
            dtm.addRow(row);
        }
        Action delete = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf(e.getActionCommand());
                DefaultTableModel model = ((DefaultTableModel) readersTable.getModel());
                Reader reader = (Reader) model.getValueAt(modelRow, 0);
                try {
                    aLibrary.removeReader(reader);
                    printReaders();
                } catch (LibraryException ex) {
                    handleError(ex.getMessage());
                }
            }
        };

        Action books = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf(e.getActionCommand());
                DefaultTableModel model = ((DefaultTableModel) readersTable.getModel());
                Reader reader = (Reader) model.getValueAt(modelRow, 0);
                processReaderBooks(reader);
            }
        };

        readersTable.setModel(dtm);
        if (dtm.getRowCount() > 0) {
            new ButtonColumn(readersTable, delete, 3);
            new ButtonColumn(readersTable, books, 2);
        }
    }

    private void processReaderBooks(Reader paReader) {
        aLogedReader = paReader;
        userBookPopup.setVisible(true);
        LinkedList<Book> borrowedBooks = paReader.getBorrowedBooks();
        Object[] row = new Object[3];
        row[0] = "Autor";
        row[1] = "Názov";
        row[2] = "";

        DefaultTableModel dtm = new DefaultTableModel();
        dtm.setColumnIdentifiers(row);
        for (Book book : borrowedBooks) {
            row = new Object[3];
            row[0] = book.getAuthor();
            row[1] = book;
            row[2] = "Vrátiť";

            dtm.addRow(row);
        }
        DefaultComboBoxModel<BookComboWrapper> dcm = new DefaultComboBoxModel<BookComboWrapper>();
        try {
            for (Book book : aLibrary.getGenreBooks(selectedGenre)) {
                if (!paReader.haveBook(book)) {
                    dcm.addElement(new BookComboWrapper(book));
                }
            }
        } catch (LibraryException ex) {
            handleError(ex.getMessage());
        }

        readerBookTable.setModel(dtm);
        availbleBooksCombo.setModel(dcm);

        Action returnBook = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf(e.getActionCommand());
                DefaultTableModel model = ((DefaultTableModel) readerBookTable.getModel());
                Book book = (Book) model.getValueAt(modelRow, 1);
                try {
                    aLibrary.returnBook(book);
                    processReaderBooks(aLogedReader);
                } catch (LibraryException ex) {
                    handleError(ex.getMessage());
                }
            }
        };

        new ButtonColumn(readerBookTable, returnBook, 2);

    }

    private void printBooks() {

        try {
            SortedLinkedList<Book> books = aLibrary.getGenreBooks(selectedGenre);
            DefaultTableModel dtm = new DefaultTableModel();
            Object[] row = new Object[5];
            row[0] = "Autor";
            row[1] = "Názov";
            row[2] = "Rok publikácie";
            row[3] = "";
            row[4] = "";
            dtm.setColumnIdentifiers(row);
            for (Book book : books) {
                row = new Object[5];
                row[0] = book.getAuthor();
                row[1] = book;
                row[2] = book.getPublicationYear();
                row[3] = "Žánre";
                row[4] = "Vymazať";
                dtm.addRow(row);
            }
            Action delete = new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int modelRow = Integer.valueOf(e.getActionCommand());
                    DefaultTableModel model = ((DefaultTableModel) bookList.getModel());
                    Book book = (Book) model.getValueAt(modelRow, 1);
                    try {
                        aLibrary.removeBook(book);
                        printBooks();
                    } catch (LibraryException ex) {
                        handleError(ex.getMessage());
                    }
                }
            };

            Action genres = new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int modelRow = Integer.valueOf(e.getActionCommand());
                    DefaultTableModel model = ((DefaultTableModel) bookList.getModel());
                    Book book = (Book) model.getValueAt(modelRow, 1);
                    processBookGenres(book);
                }
            };


            bookList.setModel(dtm);
            if (dtm.getRowCount() > 0) {
                new ButtonColumn(bookList, delete, 4);
                new ButtonColumn(bookList, genres, 3);
            }



        } catch (LibraryException libraryException) {
            handleError(libraryException.getMessage());
        }
    }

    private void processBookGenres(Book paBook) {
        bookGenresPopup.setVisible(true);
        selectedBook = paBook;
        DefaultTableModel dtm = new DefaultTableModel();

        LinkedList<BookGenre> genres = aLibrary.getGenres();
        Object[] row = new Object[2];
        row[0] = "Názov";
        row[1] = "";
        dtm.setColumnIdentifiers(row);
        for (BookGenre genre : paBook.getGenres()) {

            row = new Object[2];
            row[0] = genre;
            row[1] = "Vymazať";
            dtm.addRow(row);


        }
        DefaultComboBoxModel<BookGenre> dcm = new DefaultComboBoxModel<>();
        for (BookGenre genre : genres) {
            try {
                if (!selectedBook.haveGenre(genre)) {
                    dcm.addElement(genre);
                }
            } catch (LibraryException ex) {
                handleError(ex.getMessage());
            }
        }
        Action delete = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf(e.getActionCommand());
                DefaultTableModel model = ((DefaultTableModel) bookGenresTable.getModel());
                BookGenre genre = (BookGenre) model.getValueAt(modelRow, 0);
                try {
                    aLibrary.removeBookGenre(selectedBook, genre);
                    printBooks();
                    printGenres();
                    processBookGenres(selectedBook);
                } catch (LibraryException ex) {
                    handleError(ex.getMessage());
                }
            }
        };
        bookGenreCombo.setModel(dcm);
        bookGenresTable.setModel(dtm);
        if (dtm.getRowCount() > 0) {
            new ButtonColumn(bookGenresTable, delete, 1);
        }

    }

    private void printGenres() {
        try {
            LinkedList<BookGenre> genres = aLibrary.getGenres();
            BookGenre[] genresArray = new BookGenre[aLibrary.getGenresCount() + 1];
            genresArray[0] = new BookGenre("Zobraziť všetko");
            int i = 1;
            for (BookGenre genre : genres) {
                genresArray[i] = genre;
                i++;
            }

            DefaultComboBoxModel<BookGenre> dcm = new DefaultComboBoxModel<>(genresArray);

            if (selectedGenre != null) {
                dcm.setSelectedItem(selectedGenre);
            }

            genreCombo.setModel(dcm);
        } catch (LibraryException ex) {
            handleError(ex.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChoserPopup = new javax.swing.JDialog();
        fileChoser = new javax.swing.JFileChooser();
        addBookPopup = new javax.swing.JDialog();
        addBookLabel = new javax.swing.JLabel();
        bookNameLabel = new javax.swing.JLabel();
        bookAuthorLabel = new javax.swing.JLabel();
        publicatinoYearLabel = new javax.swing.JLabel();
        bookName = new javax.swing.JTextField();
        bookAuthor = new javax.swing.JTextField();
        publicationYear = new javax.swing.JTextField();
        saveBookButton = new javax.swing.JButton();
        addBookGenrePopup = new javax.swing.JDialog();
        addGenreLabel = new javax.swing.JLabel();
        genreName = new javax.swing.JLabel();
        genreNameInput = new javax.swing.JTextField();
        saveGenreButton = new javax.swing.JButton();
        bookGenresPopup = new javax.swing.JDialog();
        BookGenreListLabel = new javax.swing.JLabel();
        bookGenreCombo = new javax.swing.JComboBox();
        addgenreToBookButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        bookGenresTable = new javax.swing.JTable();
        addReaderPopup = new javax.swing.JDialog();
        addReaderLabel = new javax.swing.JLabel();
        ReaderNameLabel = new javax.swing.JLabel();
        ReaderSurnameLabel = new javax.swing.JLabel();
        readerAgeLabel = new javax.swing.JLabel();
        readerFirstName = new javax.swing.JTextField();
        readerLastName = new javax.swing.JTextField();
        readerAge = new javax.swing.JTextField();
        addReaderButton = new javax.swing.JButton();
        userBookPopup = new javax.swing.JDialog();
        readerBorrowsLabel = new javax.swing.JLabel();
        availbleBooksCombo = new javax.swing.JComboBox();
        borrowButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        readerBookTable = new javax.swing.JTable();
        mainPane = new javax.swing.JTabbedPane();
        panelBooks = new javax.swing.JPanel();
        addBookButton = new javax.swing.JButton();
        genreCombo = new javax.swing.JComboBox();
        genreLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        bookList = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        searchField = new javax.swing.JTextField();
        searchButton = new javax.swing.JButton();
        panelReaders = new javax.swing.JPanel();
        addUserButton = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        readersTable = new javax.swing.JTable();
        mostWaitingButton = new javax.swing.JButton();
        mostActiveCurrentButton = new javax.swing.JButton();
        mostActiveTotalButton = new javax.swing.JButton();
        panelAdmin = new javax.swing.JPanel();
        aReturnedBooksLabel = new javax.swing.JLabel();
        aReturnedBooksCount = new javax.swing.JLabel();
        proccesReturnedButton = new javax.swing.JButton();
        aHorizontalMenu = new javax.swing.JMenuBar();
        aFileMenu = new javax.swing.JMenu();
        aSaveMenu = new javax.swing.JMenuItem();
        aOpenMenu = new javax.swing.JMenuItem();

        fileChoserPopup.setMinimumSize(new java.awt.Dimension(582, 397));
        fileChoserPopup.setResizable(false);

        fileChoser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileChoserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout fileChoserPopupLayout = new javax.swing.GroupLayout(fileChoserPopup.getContentPane());
        fileChoserPopup.getContentPane().setLayout(fileChoserPopupLayout);
        fileChoserPopupLayout.setHorizontalGroup(
            fileChoserPopupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, fileChoserPopupLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(fileChoser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        fileChoserPopupLayout.setVerticalGroup(
            fileChoserPopupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fileChoserPopupLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fileChoser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        addBookPopup.setMinimumSize(new java.awt.Dimension(582, 397));
        addBookPopup.setResizable(false);
        addBookPopup.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        addBookLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addBookLabel.setText("Pridať knihu");
        addBookPopup.getContentPane().add(addBookLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(-3, 10, 580, -1));

        bookNameLabel.setText("Názov");
        addBookPopup.getContentPane().add(bookNameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));

        bookAuthorLabel.setText("Autor");
        addBookPopup.getContentPane().add(bookAuthorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, -1));

        publicatinoYearLabel.setText("Rok publikácie");
        addBookPopup.getContentPane().add(publicatinoYearLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, -1));
        addBookPopup.getContentPane().add(bookName, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 350, -1));
        addBookPopup.getContentPane().add(bookAuthor, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, 350, -1));
        addBookPopup.getContentPane().add(publicationYear, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, 60, -1));

        saveBookButton.setText("Uložiť");
        saveBookButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBookButtonActionPerformed(evt);
            }
        });
        addBookPopup.getContentPane().add(saveBookButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 170, -1, -1));

        addBookGenrePopup.setMinimumSize(new java.awt.Dimension(582, 152));
        addBookGenrePopup.setResizable(false);
        addBookGenrePopup.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        addGenreLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addGenreLabel.setText("Pridať žáner");
        addBookGenrePopup.getContentPane().add(addGenreLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(-3, 10, 580, -1));

        genreName.setText("Názov");
        addBookGenrePopup.getContentPane().add(genreName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));
        addBookGenrePopup.getContentPane().add(genreNameInput, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 350, -1));

        saveGenreButton.setText("Uložiť");
        saveGenreButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveGenreButtonActionPerformed(evt);
            }
        });
        addBookGenrePopup.getContentPane().add(saveGenreButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 90, -1, -1));

        bookGenresPopup.setMinimumSize(new java.awt.Dimension(500, 500));
        bookGenresPopup.setResizable(false);
        bookGenresPopup.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                bookGenresPopupComponentHidden(evt);
            }
        });
        bookGenresPopup.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BookGenreListLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        BookGenreListLabel.setText("Žánre knihy");
        bookGenresPopup.getContentPane().add(BookGenreListLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, -1));

        bookGenreCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        bookGenresPopup.getContentPane().add(bookGenreCombo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 130, -1));

        addgenreToBookButton.setText("Pridať žáner knihe");
        addgenreToBookButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addgenreToBookButtonActionPerformed(evt);
            }
        });
        bookGenresPopup.getContentPane().add(addgenreToBookButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, -1, -1));

        bookGenresTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(bookGenresTable);

        bookGenresPopup.getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 500, 430));

        addReaderPopup.setMaximumSize(new java.awt.Dimension(400, 250));
        addReaderPopup.setMinimumSize(new java.awt.Dimension(400, 250));
        addReaderPopup.setPreferredSize(new java.awt.Dimension(400, 250));
        addReaderPopup.setResizable(false);
        addReaderPopup.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        addReaderLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addReaderLabel.setText("Pridať čitateľa");
        addReaderPopup.getContentPane().add(addReaderLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, -1));

        ReaderNameLabel.setText("Meno");
        addReaderPopup.getContentPane().add(ReaderNameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));

        ReaderSurnameLabel.setText("Priezvisko");
        addReaderPopup.getContentPane().add(ReaderSurnameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        readerAgeLabel.setText("Vek");
        addReaderPopup.getContentPane().add(readerAgeLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        readerFirstName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                readerFirstNameActionPerformed(evt);
            }
        });
        addReaderPopup.getContentPane().add(readerFirstName, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 30, 250, -1));
        addReaderPopup.getContentPane().add(readerLastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 250, -1));
        addReaderPopup.getContentPane().add(readerAge, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 90, 40, -1));

        addReaderButton.setText("Pridať");
        addReaderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addReaderButtonActionPerformed(evt);
            }
        });
        addReaderPopup.getContentPane().add(addReaderButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 150, -1, -1));

        userBookPopup.setMaximumSize(new java.awt.Dimension(500, 500));
        userBookPopup.setMinimumSize(new java.awt.Dimension(500, 500));
        userBookPopup.setResizable(false);
        userBookPopup.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        readerBorrowsLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        readerBorrowsLabel.setText("Používateľove výpožičky");
        userBookPopup.getContentPane().add(readerBorrowsLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 480, -1));

        availbleBooksCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        userBookPopup.getContentPane().add(availbleBooksCombo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 390, -1));

        borrowButton.setText("Požičať si!");
        borrowButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                borrowButtonActionPerformed(evt);
            }
        });
        userBookPopup.getContentPane().add(borrowButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 40, -1, -1));

        readerBookTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(readerBookTable);

        userBookPopup.getContentPane().add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 77, 500, 420));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Knižnica 1.0");
        setMinimumSize(new java.awt.Dimension(500, 500));
        setName("LibraryApp"); // NOI18N
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        mainPane.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                mainPaneComponentShown(evt);
            }
        });

        panelBooks.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                panelBooksComponentShown(evt);
            }
        });
        panelBooks.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        addBookButton.setText("Pridať");
        addBookButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBookButtonActionPerformed(evt);
            }
        });
        panelBooks.add(addBookButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        genreCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        genreCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genreComboActionPerformed(evt);
            }
        });
        panelBooks.add(genreCombo, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 20, 130, -1));

        genreLabel.setText("Žáner");
        panelBooks.add(genreLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 20, -1, 20));

        jScrollPane1.setViewportView(bookList);

        panelBooks.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 490, 400));

        jButton1.setText("+");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        panelBooks.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 20, 40, -1));
        panelBooks.add(searchField, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, 80, -1));

        searchButton.setText("Hľadať");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });
        panelBooks.add(searchButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 20, -1, -1));

        mainPane.addTab("Knihy", panelBooks);

        panelReaders.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                panelReadersComponentShown(evt);
            }
        });
        panelReaders.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        addUserButton.setText("Pridať");
        addUserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUserButtonActionPerformed(evt);
            }
        });
        panelReaders.add(addUserButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        readersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(readersTable);

        panelReaders.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 490, 410));

        mostWaitingButton.setText("Najviac čakajúci");
        mostWaitingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostWaitingButtonActionPerformed(evt);
            }
        });
        panelReaders.add(mostWaitingButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, -1, -1));

        mostActiveCurrentButton.setText("Má aktuálne najviac kníh");
        mostActiveCurrentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostActiveCurrentButtonActionPerformed(evt);
            }
        });
        panelReaders.add(mostActiveCurrentButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, -1));

        mostActiveTotalButton.setText("Požičal si najviac");
        mostActiveTotalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostActiveTotalButtonActionPerformed(evt);
            }
        });
        panelReaders.add(mostActiveTotalButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, -1, -1));

        mainPane.addTab("Čitatelia", panelReaders);

        panelAdmin.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                panelAdminComponentShown(evt);
            }
        });
        panelAdmin.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        aReturnedBooksLabel.setText("Počet kníh na kontrolu");
        panelAdmin.add(aReturnedBooksLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        aReturnedBooksCount.setText("0");
        panelAdmin.add(aReturnedBooksCount, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        proccesReturnedButton.setText("Spracovať");
        proccesReturnedButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proccesReturnedButtonActionPerformed(evt);
            }
        });
        panelAdmin.add(proccesReturnedButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, -1));

        mainPane.addTab("Administrácia", panelAdmin);

        getContentPane().add(mainPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 500, 480));

        aFileMenu.setText("Súbor");

        aSaveMenu.setText("Uložiť");
        aSaveMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aSaveMenuActionPerformed(evt);
            }
        });
        aFileMenu.add(aSaveMenu);

        aOpenMenu.setText("Otvoriť");
        aOpenMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aOpenMenuActionPerformed(evt);
            }
        });
        aFileMenu.add(aOpenMenu);

        aHorizontalMenu.add(aFileMenu);

        setJMenuBar(aHorizontalMenu);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void aSaveMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aSaveMenuActionPerformed
        fileChoserPopup.setVisible(true);
        saveDialog = true;
    }//GEN-LAST:event_aSaveMenuActionPerformed

    private void fileChoserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileChoserActionPerformed
        if (openDialog) {
            try {
                aLibrary = Library.getInstance(fileChoser.getSelectedFile().getAbsolutePath());
                openDialog = false;
                printBooks();
                printGenres();
            } catch (LibraryException ex) {
                handleError(ex.getMessage());

            }
        } else if (saveDialog) {
            try {
                aLibrary.saveState(fileChoser.getSelectedFile().getAbsolutePath());
                saveDialog = false;
            } catch (LibraryException ex) {
                handleError(ex.getMessage());
            }
        }
        fileChoserPopup.setVisible(false);
    }//GEN-LAST:event_fileChoserActionPerformed

    private void aOpenMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aOpenMenuActionPerformed
        fileChoserPopup.setVisible(true);
        openDialog = true;
    }//GEN-LAST:event_aOpenMenuActionPerformed

    private void panelBooksComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_panelBooksComponentShown
        printBooks();
        printGenres();
    }//GEN-LAST:event_panelBooksComponentShown

    private void genreComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genreComboActionPerformed
        if (genreCombo.getSelectedIndex() == 0) {
            selectedGenre = null;
        } else {
            selectedGenre = (BookGenre) genreCombo.getSelectedItem();
        }

        printBooks();
    }//GEN-LAST:event_genreComboActionPerformed

    private void addBookButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBookButtonActionPerformed
        addBookPopup.setVisible(true);
    }//GEN-LAST:event_addBookButtonActionPerformed

    private void saveBookButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBookButtonActionPerformed
        String author = bookAuthor.getText();
        String name = bookName.getText();
        String pYearString = publicationYear.getText();
        if (author.equals("")) {
            handleInputError("Pole autor musí byť vyplnené");
        } else if (name.equals("")) {
            handleInputError("Pole názov musí byť vyplnené");
        } else if (pYearString.equals("")) {
            handleInputError("Pole rok publikácie musí byť vyplnené");
        } else {
            int parseInt = 0;
            try {
                parseInt = Integer.parseInt(pYearString);
            } catch (NumberFormatException numberFormatException) {
                handleInputError("Rok publikácie musí byť číslo");
                return;
            }
            try {
                aLibrary.addBook(author, name, parseInt);
                addBookPopup.setVisible(false);
                printBooks();
            } catch (LibraryException ex) {
                handleError(ex.getMessage());
            }
        }
    }//GEN-LAST:event_saveBookButtonActionPerformed

    private void saveGenreButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveGenreButtonActionPerformed

        String name = genreNameInput.getText();

        if (name.equals("")) {
            handleInputError("Pole názov musí byť vyplnené");
        } else {
            try {
                aLibrary.createGenre(name);
                printGenres();
                addBookGenrePopup.setVisible(false);
            } catch (LibraryException ex) {
                handleError(ex.getMessage());
            }
        }
    }//GEN-LAST:event_saveGenreButtonActionPerformed

    public Reader getLogedReader() {
        return aLogedReader;
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        addBookGenrePopup.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        String search = searchField.getText();
        if (search.equals("")) {
            handleInputError("Nemožno hľadať prázdny názov!!!");
        } else {
            Book book = aLibrary.findBookByName(search);
            if (book == null) {
                handleInputError("Kniha " + search + " sa v tejto knižnici nenachádza");
            } else {
                handleCasualInfoPopup("Názov: " + book.getName() + "\nAutor: " + book.getAuthor() + "\nRok publikácie: " + book.getPublicationYear());
            }

        }
    }//GEN-LAST:event_searchButtonActionPerformed

    private void addgenreToBookButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addgenreToBookButtonActionPerformed
        try {
            if (bookGenreCombo.getSelectedIndex() > -1) {
                aLibrary.addBookGenre((BookGenre) bookGenreCombo.getSelectedItem(), selectedBook);
                processBookGenres(selectedBook);
                printGenres();
                printBooks();
            } else {
                handleInputError("Nie je zvolený žiaden žáner!");
            }
        } catch (LibraryException ex) {
            handleError(ex.getMessage());
        }
    }//GEN-LAST:event_addgenreToBookButtonActionPerformed

    private void bookGenresPopupComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_bookGenresPopupComponentHidden
        this.selectedBook = null;
    }//GEN-LAST:event_bookGenresPopupComponentHidden

    private void mainPaneComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_mainPaneComponentShown
        // TODO add your handling code here:
    }//GEN-LAST:event_mainPaneComponentShown

    private void panelReadersComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_panelReadersComponentShown
        printReaders();
    }//GEN-LAST:event_panelReadersComponentShown

    private void readerFirstNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_readerFirstNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_readerFirstNameActionPerformed

    private void addReaderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addReaderButtonActionPerformed
        String firstName = readerFirstName.getText();
        String lastName = readerLastName.getText();
        String readerAgeS = readerAge.getText();

        if (firstName.equals("")) {
            handleInputError("Pole meno musí byť vyplnené");
        } else if (lastName.equals("")) {
            handleInputError("Pole priezvisko musí byť vyplnené");
        } else if (readerAgeS.equals("")) {
            handleInputError("Pole vek musí byť vyplnené");
        } else {
            try {
                int readerAge = Integer.parseInt(readerAgeS);
                aLibrary.addReader(firstName, lastName, readerAge);
                addReaderPopup.setVisible(false);
                printReaders();
            } catch (NumberFormatException numberFormatException) {
                handleInputError("Pole vek musí obsahovať číslo");
            } catch (LibraryException ex) {
                handleError(ex.getMessage());
            }
        }
    }//GEN-LAST:event_addReaderButtonActionPerformed

    private void addUserButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUserButtonActionPerformed
        addReaderPopup.setVisible(true);
    }//GEN-LAST:event_addUserButtonActionPerformed

    private void borrowButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_borrowButtonActionPerformed
        if (availbleBooksCombo.getSelectedIndex() > -1) {
            Book book = ((BookComboWrapper) availbleBooksCombo.getSelectedItem()).getBook();
            try {
                if (aLibrary.borrowBook(aLogedReader, book)) {
                    processReaderBooks(aLogedReader);

                } else {
                    handleCasualInfoPopup("Bohužial kniha nie je momentálne dostupná. Bola zaradená medzi čakajúce výpožičky!");
                }
            } catch (LibraryException ex) {
                handleError(ex.getMessage());
            }
        } else {
            handleInputError("Nie je zvolená žiadna kniha");
        }
    }//GEN-LAST:event_borrowButtonActionPerformed

    private void proccesReturnedButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proccesReturnedButtonActionPerformed
        try {
            aLibrary.proccessReturnedBooks();
            handleCasualInfoPopup("Knihy boli spracované");
            panelAdminComponentShown(null);
        } catch (LibraryException ex) {
            handleError(ex.getMessage());
        }
    }//GEN-LAST:event_proccesReturnedButtonActionPerformed

    private void panelAdminComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_panelAdminComponentShown
        try {
            aReturnedBooksCount.setText("" + aLibrary.getReturnedBooksCount());
        } catch (LibraryException ex) {
            handleError(ex.getMessage());
        }
    }//GEN-LAST:event_panelAdminComponentShown

    private void mostWaitingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostWaitingButtonActionPerformed
        Reader reader = aLibrary.findMostWaitingReader();
        handleCasualInfoPopup(reader.toString() + "\nVek: " + reader.getAge() + "\nČaká na " + reader.getWaitedBooksCount() + " kníh");
    }//GEN-LAST:event_mostWaitingButtonActionPerformed

    private void mostActiveCurrentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostActiveCurrentButtonActionPerformed
        Reader reader;
        try {
            reader = aLibrary.findCurrentMostActiveReader();
            handleCasualInfoPopup(reader.toString() + "\nVek: " + reader.getAge() + "\nMá " + reader.getBorrowedBooksCount() + " kníh");
        } catch (LibraryException ex) {
            handleError(ex.getMessage());
        }

    }//GEN-LAST:event_mostActiveCurrentButtonActionPerformed

    private void mostActiveTotalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostActiveTotalButtonActionPerformed
        Reader reader;
        try {
            reader = aLibrary.findMostActiveReader();
            handleCasualInfoPopup(reader.toString() + "\nVek: " + reader.getAge() + "\nCelkovo si požičal " + reader.getBorrowedBooksTotal() + " kníh");
        } catch (LibraryException ex) {
            handleError(ex.getMessage());
        }
    }//GEN-LAST:event_mostActiveTotalButtonActionPerformed

    private void handleError(String paError) {

        JOptionPane.showMessageDialog(this, "V aplikáci nastal problém.\nVysokošpecializovaný tím cvičených opíc bol vyslaný aby sa postaral o túto situáciu. Ak ich uvidíte ukáže im túto správu:\n\n " + paError, "Chyba", JOptionPane.ERROR_MESSAGE);
    }

    private void handleInputError(String paError) {

        JOptionPane.showMessageDialog(this, paError, "Chyba", JOptionPane.ERROR_MESSAGE);
    }

    private void handleCasualInfoPopup(String paMessage) {
        JOptionPane.showMessageDialog(genreCombo, paMessage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LibraryApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LibraryApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LibraryApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LibraryApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LibraryApp().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BookGenreListLabel;
    private javax.swing.JLabel ReaderNameLabel;
    private javax.swing.JLabel ReaderSurnameLabel;
    private javax.swing.JMenu aFileMenu;
    private javax.swing.JMenuBar aHorizontalMenu;
    private javax.swing.JMenuItem aOpenMenu;
    private javax.swing.JLabel aReturnedBooksCount;
    private javax.swing.JLabel aReturnedBooksLabel;
    private javax.swing.JMenuItem aSaveMenu;
    private javax.swing.JButton addBookButton;
    private javax.swing.JDialog addBookGenrePopup;
    private javax.swing.JLabel addBookLabel;
    private javax.swing.JDialog addBookPopup;
    private javax.swing.JLabel addGenreLabel;
    private javax.swing.JButton addReaderButton;
    private javax.swing.JLabel addReaderLabel;
    private javax.swing.JDialog addReaderPopup;
    private javax.swing.JButton addUserButton;
    private javax.swing.JButton addgenreToBookButton;
    private javax.swing.JComboBox availbleBooksCombo;
    private javax.swing.JTextField bookAuthor;
    private javax.swing.JLabel bookAuthorLabel;
    private javax.swing.JComboBox bookGenreCombo;
    private javax.swing.JDialog bookGenresPopup;
    private javax.swing.JTable bookGenresTable;
    private javax.swing.JTable bookList;
    private javax.swing.JTextField bookName;
    private javax.swing.JLabel bookNameLabel;
    private javax.swing.JButton borrowButton;
    private javax.swing.JFileChooser fileChoser;
    private javax.swing.JDialog fileChoserPopup;
    private javax.swing.JComboBox genreCombo;
    private javax.swing.JLabel genreLabel;
    private javax.swing.JLabel genreName;
    private javax.swing.JTextField genreNameInput;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane mainPane;
    private javax.swing.JButton mostActiveCurrentButton;
    private javax.swing.JButton mostActiveTotalButton;
    private javax.swing.JButton mostWaitingButton;
    private javax.swing.JPanel panelAdmin;
    private javax.swing.JPanel panelBooks;
    private javax.swing.JPanel panelReaders;
    private javax.swing.JButton proccesReturnedButton;
    private javax.swing.JLabel publicatinoYearLabel;
    private javax.swing.JTextField publicationYear;
    private javax.swing.JTextField readerAge;
    private javax.swing.JLabel readerAgeLabel;
    private javax.swing.JTable readerBookTable;
    private javax.swing.JLabel readerBorrowsLabel;
    private javax.swing.JTextField readerFirstName;
    private javax.swing.JTextField readerLastName;
    private javax.swing.JTable readersTable;
    private javax.swing.JButton saveBookButton;
    private javax.swing.JButton saveGenreButton;
    private javax.swing.JButton searchButton;
    private javax.swing.JTextField searchField;
    private javax.swing.JDialog userBookPopup;
    // End of variables declaration//GEN-END:variables
}
