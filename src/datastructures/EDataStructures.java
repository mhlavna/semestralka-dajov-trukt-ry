package datastructures;

/**
 *
 * @author Michal Varga
 */
public class EDataStructures extends Exception {
    
    public EDataStructures(String paMessage) {
        super(paMessage);
    }
    
}
