/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.priorityFronts;

import java.io.Serializable;

/**
 *
 * @author Martin Hlavňa
 */
public class priorityNode<E> implements Comparable<priorityNode<E>>, Serializable {

    private int aPriority;
    private E aData;

    public priorityNode(E data, int aPriority) {
        this.aData = data;
        this.aPriority = aPriority;
    }

    public E getData() {
        return aData; //To change body of generated methods, choose Tools | Templates.
    }

    public int getPriority() {
        return aPriority;
    }

    @Override
    public int compareTo(priorityNode<E> paNode) {
        if (aPriority > paNode.aPriority) {
            return 1;
        }
        if (aPriority < paNode.aPriority) {
            return -1;
        }
        return 0;
    }
}
