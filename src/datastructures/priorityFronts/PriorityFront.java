package datastructures.priorityFronts;

import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;
import datastructures.lists.LinkedList.SortedLinkedList;
import java.io.Serializable;
import java.util.Iterator;

/**
 * Implementation fo priority fron based on double list
 *
 * @author Martin Hlavňa
 */
public class PriorityFront<E> implements IPriorityFront<E>, Serializable {

    private SortedLinkedList<priorityNode<E>> aShort;
    private LinkedList<priorityNode<E>> aLong;
    private int aPmax;
    private boolean isShortFull;
    private int aCount;

    public PriorityFront() {
        aShort = new SortedLinkedList<>();
        aLong = new LinkedList<>();
    }

    @Override
    public void push(E paElement, int paPriority) throws EPriorityFront {
        priorityNode<E> newNode = new priorityNode<>(paElement, paPriority);

        if (isShortFull) {
            if (paPriority <= aPmax) {
                if (isShortFull()) {
                    try {
                        priorityNode<E> pom = aShort.deleteFromIndex(aShort.size() - 1);
                        aLong.add(pom);
                        aShort.add(pom);
                    } catch (EList ex) {
                        throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
                    }
                } else {
                    addToShort(newNode);
                }
            } else {
                try {
                    aLong.add(newNode);
                } catch (EList ex) {
                    throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
                }
            }
        } else {
            addToShort(newNode);
        }
        try {
            aPmax = aShort.get(aShort.size() - 1).getPriority();
        } catch (EList ex) {
            throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
        }
        aCount++;

    }

    /**
     * Checks if short list is full
     *
     * @return true if yes false if no
     * @throws EPriorityFront If there is problem in underlying structure
     */
    private boolean isShortFull() throws EPriorityFront {
        try {
            return this.aShort.size() > Math.sqrt(this.size());
        } catch (EList ex) {
            throw new EPriorityFront("Problem checking if priorityFront short List is full caused by: " + ex.getMessage());
        }
    }

    @Override
    public E pop() throws EPriorityFront {
        try {
            priorityNode<E> toDelete = aShort.deleteFromIndex(0);
            aCount--;
            if (aShort.size() == 0) {
                this.isShortFull = false;
                LinkedList<priorityNode<E>> buffer = aLong;
                aCount = 0;
                aLong = new LinkedList<>();
                for (priorityNode<E> ppf : buffer) {
                    push(ppf.getData(), ppf.getPriority());
                }
            }

            return toDelete.getData();

        } catch (EList ex) {
            throw new EPriorityFront("Problem poping from priorityFront caused by: " + ex.getMessage());
        }
    }

    @Override
    public E peek() throws EPriorityFront {
        try {
            return aShort.get(0).getData();
        } catch (EList ex) {
            throw new EPriorityFront("Problem peeking from priorityFront caused by: " + ex.getMessage());
        }
    }

    @Override
    public boolean isEmpty() throws EPriorityFront {

        return aCount == 0;

    }

    @Override
    public int size() throws EPriorityFront {

        return aCount;

    }

    @Override
    public void clear() throws EPriorityFront {

        aShort = new SortedLinkedList<>();
        aLong = new LinkedList<>();
        aPmax = 0;
        aCount = 0;
    }

    @Override
    public Iterator<E> iterator() {
        return null; //v aplikácii Knižnica nie je potrebný
    }

    /**
     * Adds new node to the short list
     *
     * @param newNode Node to add
     * @throws EPriorityFront If there is problem with underlying structure
     */
    private void addToShort(priorityNode<E> newNode) throws EPriorityFront {
        try {
            aShort.add(newNode);
            if (isShortFull()) {
                isShortFull = true;
            }
        } catch (EList ex) {
            throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
        }
    }
}
