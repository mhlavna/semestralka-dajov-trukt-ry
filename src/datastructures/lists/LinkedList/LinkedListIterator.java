package datastructures.lists.LinkedList;

import java.util.Iterator;

/**
 * Iterator for the LinkedList
 *
 * @author Martin Hlavňa
 */
public class LinkedListIterator<E> implements Iterator<E> {

    private LinkedList<E> list;
    private LinkedListChunk current;

    /**
     * Creates new Linked List
     *
     * @param list
     */
    public LinkedListIterator(LinkedList list) {
        this.current = list.aFirstChunk;
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        if (current != null) {
            return true;
        }
        return false;
    }

    @Override
    public E next() {
        E output = (E) current.getData();
        current = current.getNext();
        return output;

    }

    @Override
    public void remove() {
    }
}
