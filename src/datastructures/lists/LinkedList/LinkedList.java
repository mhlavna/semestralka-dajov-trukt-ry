package datastructures.lists.LinkedList;

import datastructures.lists.EList;
import datastructures.lists.IList;
import java.io.Serializable;
import java.util.Iterator;

/**
 * Implementation of the LinkedList data structure.
 *
 * @author Martin Hlavňa
 */
public class LinkedList<E> implements IList<E>, Serializable {

    protected LinkedListChunk<E> aFirstChunk;
    protected LinkedListChunk<E> aLastChunk;
    protected int aCount;

    public LinkedList() {
        aLastChunk = aFirstChunk = null;
        aCount = 0;
    }

    @Override
    public void add(E paElement) throws EList {
        LinkedListChunk<E> newChunk = new LinkedListChunk<>(paElement);
        if (aLastChunk == null) {
            aFirstChunk = newChunk;
        } else {
            aLastChunk.setNext(newChunk);
        }

        aLastChunk = newChunk;
        aCount++;
    }

    @Override
    public void set(int paIndex, E paElement) throws EList {
        insert(paElement, paIndex);
    }

    @Override
    public void insert(E paElement, int paIndex) throws EList {
        if (paIndex == aCount) {
            this.add(paElement);
        } else if (paIndex == 0) {
            LinkedListChunk<E> newChunk = new LinkedListChunk<>(paElement);
            newChunk.setNext(aFirstChunk);
            aFirstChunk = newChunk;
            aCount++;
        } else {
            LinkedListChunk<E> prev = getChunkAtIndex(paIndex - 1);
            LinkedListChunk<E> newChunk = new LinkedListChunk<>(paElement);
            newChunk.setNext(prev.getNext());
            prev.setNext(newChunk);
            aCount++;
        }

    }

    @Override
    public E delete(E paElement) throws EList {
        int index = indexOf(paElement);
        if (index == -1) {
            return null;
        }

        deleteFromIndex(index);
        return paElement;
    }

    @Override
    public E deleteFromIndex(int paIndex) throws EList {
        if (paIndex >= 0 && paIndex < aCount) {
            LinkedListChunk<E> chunkAtIndex;
            if (paIndex == 0) {
                chunkAtIndex = aFirstChunk;
                aFirstChunk = aFirstChunk.getNext();
                if (aFirstChunk == null) {
                    aLastChunk = null;
                }
            } else if (paIndex == aCount - 1) {
                LinkedListChunk<E> prevChunkAtIndex = getChunkAtIndex(paIndex - 1);
                chunkAtIndex = prevChunkAtIndex.getNext();
                prevChunkAtIndex.setNext(null);
                aLastChunk = prevChunkAtIndex;
            } else {
                chunkAtIndex = getChunkAtIndex(paIndex - 1);
                chunkAtIndex.setNext(chunkAtIndex.getNext().getNext());
            }
            aCount--;
            return chunkAtIndex.getData();
        }
        throw new EList("Incorect bounds(" + paIndex + ") specified. Correct bounds are from 0 to " + (aCount - 1));
    }

    @Override
    public int indexOf(E paElement) throws EList {
        LinkedListChunk<E> chunk = aFirstChunk;

        for (int i = 0; i < aCount; i++) {


            if (chunk.getData() == paElement) {
                return i;
            }
            chunk = chunk.getNext();
        }

        return -1;


    }

    @Override
    public E get(int paIndex) throws EList {
        return getChunkAtIndex(paIndex).getData();
    }

    @Override
    public int size() throws EList {
        return aCount;
    }

    @Override
    public void clear() throws EList {
        aLastChunk = aFirstChunk = null;
        aCount = 0;
    }

    protected LinkedListChunk<E> getChunkAtIndex(int paIndex) throws EList {

        if (paIndex >= 0 && paIndex < aCount) {
            if (paIndex == aCount - 1) {
                return aLastChunk;
            }
            LinkedListChunk<E> chunk = aFirstChunk;

            for (int i = 0; i < paIndex; i++) {
                chunk = chunk.getNext();
            }
            return chunk;
        }

        throw new EList("Incorect bounds specified. Correct bounds are from 0 to " + (aCount - 1));
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<>(this);
    }
}
